<?php

	require 'Config.php';
	require 'Library/JWT.php';
	require 'Service/CurlUser.php';
	require 'Model/Connect.php';
	require 'Model/ModelUser.php';
	require 'Entities/UserEntities.php';

	$code = isset($_GET['code']) ? $_GET['code'] : '';

	if ( $code == '' ) {
		header('Location: '.Config::URL);
	} else {
		
		$getToken = getDataGithub($code);
		$token = getTokenFromString($getToken);

		$curlUser = new CurlUser();
		
		$curlUser->setValue($token);
		$infoUser = $curlUser->infoUser();

		if ( $infoUser['status'] === 1 ) {

			$connectDb = new Connect();
			$userRepos = new UserEntities($infoUser['avatar'],$infoUser['id_user'],$infoUser['node_id'],$infoUser['username']);

			$connect = $connectDb->connect();
			$modelUser = new ModelUser($connect);

			if ( $modelUser->insertUser($userRepos) ) {
				$tokenSetCookie = array(
					'tokenUser' => $token,
					'id_user' => $infoUser['id_user'],
				);

				$jwt = new JWT();
				$jwtEncode = $jwt->encode($tokenSetCookie);

				setcookie('token_user',$jwtEncode,0,'/');
				setcookie('username',$infoUser['username']);
				setcookie('id_user',$infoUser['id_user']);
				setcookie('avatar',$infoUser['avatar']);

				header('Location: '.Config::URL);
			}
		} else {
			echo $infoUser['message'];
		}
	}
	

	function getDataGithub($code) {
		
		$curl = curl_init();
		curl_setopt_array($curl, array(
		CURLOPT_RETURNTRANSFER => 1,
		CURLOPT_URL => 'https://github.com/login/oauth/access_token?client_id='.Config::CLIENT_ID_GITHUB.'&client_secret='.Config::PRIVATE_KEY_GITHUB.'&code='.$code,
		CURLOPT_USERAGENT => 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:47.0) Gecko/20100101 Firefox/47.0',
		CURLOPT_SSL_VERIFYPEER => false
		));

		$resp = curl_exec($curl);
		curl_close($curl);
		return $resp;
	}
	// access_token=382c7219faf5c445ac2e33beb7e9e3657f7f2dbf&scope=&token_type=bearer1

	function getTokenFromString($string) {

		if (preg_match("/access_token=(.+)&scope/", $string,$match))
		  {
		    	return $match[1];
		  }
		  return '1';
	}

