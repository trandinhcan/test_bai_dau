var path = require("path");
var express = require("express");
var app = express();
var server = require('http').createServer(app);
var io = require('socket.io')(server);
var mysql = require('mysql');
const request = require('request');
var https = require('https');
var querystring = require('querystring');

var connectDb = mysql.createConnection({
  host: 'localhost',
  user: "root",
  password: '',
  database : 'test'
});

// var token = '7386b299b557fbe044c374be5ea85f4fd994163f';
// var repos = 'wordpress-mobile/AztecEditor-iOS'; // usrename fork


var options,req;

io.on('connection', function (socket) {
    socket.on('fork',function(data){
        if ( data.idRepos != undefined && data.nameRepos != undefined && data.token != undefined ) {
            options = {
              hostname: 'api.github.com',
              path: '/repos/'+data.nameRepos+'/forks',
              method: 'POST',
              headers: {
                'User-Agent': 'http://developer.github.com/v3/',
                'Authorization': 'token ' +data.token,
              }
            };
            let result = '';
            req = https.request(options, (res) => {
              res.setEncoding('utf8');

              let rece = '' ;

              res.on('data', (chunk) => {
                rece += chunk;
              });
              res.on('end', () => {
                try{
                    let decode = JSON.parse(rece);
                    if ( decode.full_name != undefined ) {
                        processSuccess(socket,data.idRepos,decode.full_name);
                    } else {
                        requestFail(socket,'Fail !');
                    }
                } catch(ex){
                    requestFail(socket,'Fail!');
                }
              });
            });

            req.on('error', (e) => {
                console.log(e);
            });
            req.end();
        }
    });

});

function requestFail(socket,message){
    socket.emit('fork_fail',{'message':message});
}

function processSuccess(socket,idRepos,full_name){
    socket.emit('fork_success',{'idRepos':idRepos,'full_name':full_name});
    connectDb.query('UPDATE repos SET is_fork = 1,url_fork="'+full_name+'" WHERE idRepos="'+idRepos+'"', function ( err,result) {});
}
server.listen(3000);

