<?php
	
	require 'Library/JWT.php';
	require 'Service/CurlUser.php';
	require 'Service/Validate.php';
	require 'Model/Connect.php';
	require 'Model/ModelUser.php';
	require 'Entities/ReposEntities.php';

	class ControllerUser{
		
		private $token_user;
		private $curlUser;
		private $id_user;

		function __construct($cookie,$tl1,$tl2) {
			$this->getTokenFromCookie($cookie);
			$this->curlUser = new CurlUser();
			$this->curlUser->setValue($this->token_user,Config::REPOS_PER_REQUEST);


			if ($tl1 == '') {
				$this->dashboard();
			} else if ( $tl1 == 'repos-username' ) {
				$page = isset($_GET['tl3']) ? $_GET['tl3'] : 1;
				$page = is_numeric($page) ? $page : 1;

				$this->getInfoUser($tl2,$page);
			} else if ( $tl1 === 'save-info-repos' ) {
				$this->saveInfoRepos();
			} else if ( $tl1 == 'repos-saved' ) {
				$this->repos_saved();
			}
		}

		private function dashboard() {
			require 'View/User/Dashboard.php';
		}

		private function getInfoUser($username,$page) {
			echo $this->curlUser->getInfoUserFromUsername($username,$page);
		}

		private function saveInfoRepos() {
			$validate = new Validate();

			if ( !$validate->issetParamSaveRepos() ) {
				echo json_encode(['status'=>0,'message'=>'Không đủ dữ liệu']);
				return;
			} 

			if( !$validate->validateParamSvaeRepos() ) {
				echo json_encode(['status'=>0,'message'=>'Dữ liệu không đúng']);
				return;
			}

			$reposEntities = new ReposEntities($_POST['id_repos'],$_POST['name_repos'],$_POST['created_at'],$_POST['username'],$_POST['stargazers_count'],$_POST['description']);
			
			$connectDb = new Connect();
			$connect = $connectDb->connect();

			$model = new ModelUser($connect);
			$saveRepos = $model->saveRepos($reposEntities,$this->id_user);

			if ( $saveRepos ) {
				echo json_encode(['status'=>1,'message'=>'Thành Công']);
			} else {
				echo json_encode(['status'=>0,'message'=>'Lỗi CSDL hoặc Repos đã tồn tại']);
			}
			$connect->close();
		}

		private function repos_saved() {

			$page = isset($_GET['tl2']) ? $_GET['tl2'] : 1;
			$page = is_numeric($page) ? $page : 1;

			$connectDb = new Connect();
			$connect = $connectDb->connect();

			$model = new ModelUser($connect);

			$listRepos = $model->listReposSaved($page,$this->id_user);
			$totalRepos = $model->TotalReposSaved($this->id_user);

			$model->closeConnect();

			require 'View/User/ReposSave.php';
		}



		private function getTokenFromCookie($cookie) {
			
			$jwt = new JWT();

			$jwtDecodeCookie = $jwt->decode($cookie);
			if ( $jwtDecodeCookie === false ) {

			} else {
				$this->token_user = $jwtDecodeCookie->tokenUser;
				$this->id_user = $jwtDecodeCookie->id_user;				
			}
		}
	}