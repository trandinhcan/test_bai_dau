<?php

	class Connect{


		public function connect() {
			$connect = new mysqli(Config::HOST_DB,Config::USERNAME_DB,Config::PASSWORD_DB,Config::DB_NAME) or die('not connect DB!');
			$connect->query("SET NAMES utf8");

			return $connect;
		}

	}