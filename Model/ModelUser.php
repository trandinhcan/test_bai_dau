<?php
	class ModelUser {

		private $connectDb;
		private $viewPerPage;

		public function __construct($connect,$viewPerPage = 30) {
			$this->connectDb = $connect;
			$this->viewPerPage = $viewPerPage;
		}
 
		public function saveRepos(ReposEntities $repoEntities,int $id_user) {
			if (  $stmt= $this->connectDb->prepare('INSERT INTO repos(idRepos,nameRepos, create_at,username,stargazers_count,description,id_user) VALUES(?,?,?,?,?,?,?)') ) {
 
				$stmt->bind_param('ssssisi',$idRepos,$nameRepos,$create_at,$username,$stargazers_count,$description,$id_user);

				$idRepos = $repoEntities->getIdRepos();
				$nameRepos = $repoEntities->getNameRepos();
				$create_at = $repoEntities->getCreate_at();
				$username = $repoEntities->getUsername();
				$stargazers_count = $repoEntities->getStargazers_count();
				$description = $repoEntities->getDescription();

				$stmt->execute();
				$rowInsert = $stmt->affected_rows;
				$stmt->close();

				if ( $rowInsert === 1 ) {
					return true;
				}
			} 
			return false;
		}

		public function TotalReposSaved(int $id_user){
			$query = $this->connectDb->query('SELECT count(id) AS count FROM repos WHERE id_user='.$id_user);
			if( $row = $query->fetch_assoc() ) {
				return $row['count'];
			}
			return 0;
		}

		public function listReposSaved(int $page, int $id_user){
			$start = ($page-1) * $this->viewPerPage;
			return $this->connectDb->query('SELECT * FROM repos WHERE id_user= '.$id_user.' LIMIT '.$start.','.$this->viewPerPage);
		}

		public function insertUser($userEntities) {
			$query = $this->connectDb->query("SELECT count('id') AS count FROM users_repos WHERE id_user=".$userEntities->getId_user() );
			if ( $row = $query->fetch_assoc() ) {
				if ( $row['count'] == 1 ) {
					return true;
				}
			}

			if ( $stmt = $this->connectDb->prepare("INSERT INTO users_repos(avatar,id_user,node_id,username) VALUES(?,?,?,?)") ) {
				$stmt->bind_param("siss",$avatar,$id_user,$node_id,$username );

				$avatar = $userEntities->getAvatar();
		 		$id_user = $userEntities->getId_user();
			 	$node_id = $userEntities->getNode_id();
				$username = $userEntities->getUsername();

				$stmt->execute();
				$stmt->close();
				return true;
			}
			return false;

		}

		public function closeConnect(){
			$this->connectDb->close();
		}


	}