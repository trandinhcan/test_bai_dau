
<!DOCTYPE html>
<!--
-->
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title> Github </title>
  <base href="<?php  echo Config::URL; ?>">

  <link rel="stylesheet" href="style/css/all.min.css">
  <link rel="stylesheet" href="style/css/adminlte.min.css">
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">

  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
    </ul>


 <!--    <ul class="navbar-nav ml-auto">
     
      <li class="nav-item dropdown">
        <a class="nav-link" style="color: red">
          20.5 <i class="fas fa-dollar-sign"></i>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#" role="button"><i
            class="fas fa-th-large"></i></a>
      </li>
    </ul> --> 
  </nav>
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <a href="" class="brand-link">
      <i class="fas fa-house-user"></i>
      <span class="brand-text font-weight-light" style="padding-left: 5px ">Github</span>
    </a>

    <div class="sidebar">
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
       

          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="fas fa-house-user"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>

          <li class="nav-item">
            <a href="repos-saved.html" class="nav-link">
              <i class="nav-icon fas fa-th"></i>
              <p>
                List Repos Đã Lưu
              </p>
            </a>
          </li>
        </ul>
      </nav>
    </div>
  </aside>

  <div class="content-wrapper">
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Repos Đã Lưu</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="">Trang chủ</a></li>
              <li class="breadcrumb-item active">Repos Đã Lưu</li>
            </ol>
          </div>
        </div>
      </div>
    </div>

    <div class="content">
      <div class="container-fluid">
            <div class="card">
              <div class="card-body">
                <!-- <h5 class="card-title">Repository đã Lưu </h5> -->
                <br><hr>
                <p class="card-text">
                  <div class="row list_repos">
                    <div style="margin-top:10px"></div>
                    <?php

                      while( $row = $listRepos->fetch_assoc() ) {
                        if ( !$row['is_fork'] ) {
                          $fork = '<button onclick="fork(this)" idRepos="'.$row['idRepos'].'" pathFork="'.$row['username'].'/'.$row['nameRepos'].'" class="btn btn-primary fork">Fork</button>';
                        } else {  
                          $fork = '<a target="_blank" href="https://github.com/'.$row['url_fork'].'"><button class="btn btn-success">Link Fork Repos</button></a>';
                        }
                        echo '  <div class="col-12"><div style="float:left;width:80%" class="left"><h4>'.$row['nameRepos'].'</h4><h6>'.$row['description'].'</h6><span title="username" >'.$row['username'].'</span> - <span title="Ngày tạo">'.$row['create_at'].'</span></div><div style="float:right;width: 20%">stargazers : <span id="stargazers" style="color:green">'.$row['stargazers_count'].'</span><br>'.$fork.'</div><div class="clearfix"></div><hr></div>';
                      }
                    ?>
                  </div>
                </p>
              </div>
                <?php   echo  panigation($page,$totalRepos,Config::VIEW_PER_PAGE); ?>
            </div>
          
          
      </div>
    </div>
  </div>
</div>
<script src="style/js/socket.io.js"></script>

<script >
  var token = "<?php echo $this->token_user; ?>";
  var client = io("<?php  echo Config::HOST_NODEJS; ?>");

</script>
<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
<script src="style/js/notify.js"></script>
<script src="style/js/adminlte.min.js"></script>
<script src="style/js/fork.js"></script>

<style>
  .list_repos .left span{
    font-size: 12px;
  }
  .list_repos a{
    color:white;
  }
  .alert{
    position: fixed;
    width: 200px;
    height: 100px;
    background: white;
    border: 1px solid grey;
    bottom : 20px;
    left: 10px;
    z-index: 1000000;
    text-align: center;
    cursor: pointer;
  }
</style>

</body>
</html>

<?php
  function panigation($pageCurrent,$totalRecords,$viewPerPage = 30){
        $result = '';
        $totalPage = ceil($totalRecords/$viewPerPage);
        $tmp = floor($pageCurrent/5) * 5;
        if( $tmp == $pageCurrent ){
            $beginPagination = $tmp - 5 + 1;
        }else{
            $beginPagination = $tmp + 1;
        }
        $endPagination = $beginPagination  + 5 - 1;
        if($endPagination > $totalPage){
            $endPagination = $totalPage;
        }

        if($beginPagination > 5 ){
            $result .= '<li class="page-item"><a href="repos-saved/'.($beginPagination - 1).'.html">&larr; Trang trước</a></li>';
        }

            for($i = $beginPagination;$i <= $endPagination ;$i++){
                 if($i == $pageCurrent){
                    $result.='<li class="active"><a class="page-link" rel="nofollow">'.$i.'</a></li>';
                }else{
                    $result .='<li class="page-item"><a rel="nofollow" class="page-link" href="repos-saved/'.$i.'.html">'.$i.'</a></li>';
                }
            }
        if($endPagination < $totalPage ){
            $result .='<li class="page-item"><a class="page-link" href="repos-saved/'.($endPagination + 1).'.html">Next</a></li>';
        }
        
        return '<nav aria-label="Page navigation example"><ul class="pagination justify-content-center">'.$result.'</ul></nav>';
    }