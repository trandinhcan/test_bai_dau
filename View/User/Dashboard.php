
<!DOCTYPE html>
<!--
-->
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title> Github </title>
  <base href="<?php  echo Config::URL; ?>">

  <link rel="stylesheet" href="style/css/all.min.css">
  <link rel="stylesheet" href="style/css/adminlte.min.css">
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">

  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
    </ul>


  </nav>
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <a href="" class="brand-link">
      <i class="fas fa-house-user"></i>
      <span class="brand-text font-weight-light" style="padding-left: 5px ">Github</span>
    </a>

    <div class="sidebar">
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
       

          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="fas fa-house-user"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>

          <li class="nav-item">
            <a href="repos-saved.html" class="nav-link">
              <i class="nav-icon fas fa-th"></i>
              <p>
                List Repos Đã Lưu
              </p>
            </a>
          </li>
        </ul>
      </nav>
    </div>
  </aside>

  <div class="content-wrapper">
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Dashboard</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Trang chủ</a></li>
              <li class="breadcrumb-item active">Dashboard</li>
            </ol>
          </div>
        </div>
      </div>
    </div>

    <div class="content">

     
      <div class="container-fluid">


         <div class="row"> 
          <div class="col-1"></div>
            <div class="card col-5">
              <div class="card-body">
                <!-- <h5 class="card-title">Thông tin người dùng  </h5> -->
                <p class="card-text">
                  <div class="row">
                    <div class="col-6"> 
                      
                    </div>
                  </div>
                    <h5 class="username" style="font-size:15px"> Username : <span style="color:green"></span>  </h5>
                    <br>
                </p>
              </div>
            </div>
          <div class="col-1"></div>
            <div class="card col-5">
              <div class="card-body">
                <!-- <h5 class="card-title">Thông tin người dùng  </h5> -->
                <p class="card-text">
                  <div class="row">
                    <div class="col-6"> 
                      
                    </div>
                  </div>
                    <h5 class="id_user" style="font-size:15px"> Id user : <span style="color:green"></span>  </h5>
                    <br>
                </p>
              </div>
            </div>


          </div>
          

            <div class="card">
              <div class="card-body">
                <h5 class="card-title">Lấy thông tin repository người dùng  </h5>

                <p class="card-text">
                  <div class="row">
                    <div class="col-md-9">
                      <input type="text" class="form-control usernameGithub" placeholder="Username Github">
                    </div>
                    <div class="col-md-3"><button class="btn btn-primary getReposUser">Lấy thông tin</button></div>
                  </div>
                </p>
              </div>
            </div>

            <div class="card">
              <div class="card-body">
                <h5 class="card-title"> Số repository đã load: <span id="reposLoadcount" style="color:green">0 </h5>
                <p class="card-text"></span></p>
                <p class="card-text">
                  <hr/>
                  <div class="row" id="response">
                   
                  </div>
                </p>
                <p class="card-text" id="loadMore">
                  
                </p>
                
              </div>
            </div>
      </div>
    </div>
  </div>
</div>

<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
<script src="style/js/notify.js"></script>
<script src="style/js/adminlte.min.js"></script>
<script src="style/js/dashboard.js"></script>


</body>
</html>
