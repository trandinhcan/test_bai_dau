-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: May 08, 2020 at 12:58 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `test`
--

-- --------------------------------------------------------

--
-- Table structure for table `repos`
--

CREATE TABLE `repos` (
  `id` int(11) NOT NULL,
  `idRepos` varchar(10) DEFAULT NULL,
  `nameRepos` varchar(255) DEFAULT NULL,
  `create_at` varchar(20) DEFAULT NULL,
  `username` varchar(20) DEFAULT NULL,
  `stargazers_count` int(11) DEFAULT NULL,
  `description` varchar(255) NOT NULL,
  `is_fork` tinyint(1) DEFAULT 0,
  `url_fork` varchar(250) DEFAULT NULL,
  `id_user` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `repos`
--

INSERT INTO `repos` (`id`, `idRepos`, `nameRepos`, `create_at`, `username`, `stargazers_count`, `description`, `is_fork`, `url_fork`, `id_user`) VALUES
(45, '197457217', '.github', '2019-07-17T20:22:09Z', 'psf', 0, 'Organization-wide GitHub settings', 1, 'trancan97/.github', 64890397),
(46, '125266328', 'black', '2018-03-14T19:54:45Z', 'psf', 0, 'The uncompromising Python code formatter', 1, 'trancan97/black', 64890397),
(48, '62902961', 'XYZ-ReaderApp', '2016-07-08T16:38:28Z', 'geniushkg', 0, 'Material Design Make over of News reader Android APP', 1, 'trancan97/XYZ-ReaderApp', 64890397),
(49, '77360403', 'spoj', '2016-12-26T05:37:21Z', 'geniushkg', 0, 'Competitive Programming questions solved in java', 1, 'trancan97/spoj', 64890397),
(50, '59890132', 'geniushkg.github.io', '2016-05-28T10:53:29Z', 'geniushkg', 0, 'Random rants and wisdom', 0, NULL, 64890397);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `repos`
--
ALTER TABLE `repos`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `idRepos` (`idRepos`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `repos`
--
ALTER TABLE `repos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
