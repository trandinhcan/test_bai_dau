<?php
 // encode algorith SH256
class JWT
{
    private $key = "_github";

    public function decode($jwt){ 
        $tks = explode('.', $jwt);
        if (count($tks) != 2) return false;
        list($headb64, $bodyb64, $cryptob64) = ['eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9',$tks[0],$tks[1]];
        if (null === ($header = json_decode($this->urlsafeB64Decode($headb64), false, 512, 2)))return false;
        if (null === $payload = json_decode($this->urlsafeB64Decode($bodyb64), false, 512, 2))return false;
        if (false === ($sig = $this->urlsafeB64Decode($cryptob64))) return false;
        if( ! hash_equals($sig, hash_hmac('SHA256', "$headb64.$bodyb64", $this->key, true)))return  false;
        // if(!isset($payload->expired)  || $payload->expired < time() )return false;
        return $payload;
    }
    public function encode($payload){
        $header = array(
            'typ' => 'JWT',
            'alg' => 'HS256',
        );
        $segments      = array();
        $segments[]    = $this->urlsafeB64Encode(json_encode($header));
        $segments[]    = $this->urlsafeB64Encode(json_encode($payload));
        $signing_input = implode('.', $segments);
        
        $signature  = hash_hmac('SHA256', $signing_input, $this->key, true);
        $segments[] = $this->urlsafeB64Encode($signature);
        
        return $segments[1]."." .$segments[2];
    }
    
    private function urlsafeB64Decode($input){
        $remainder = strlen($input) % 4;
        if ($remainder) {
            $padlen = 4 - $remainder;
            $input .= str_repeat('=', $padlen);
        }
        return base64_decode(strtr($input, '-_', '+/'));
    }
    private function urlsafeB64Encode($input){
        return str_replace('=', '', strtr(base64_encode($input), '+/', '-_'));
    }
}