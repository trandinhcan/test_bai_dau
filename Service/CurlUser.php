<?php

	class CurlUser {

		private $token,$reposPerRequest;

		public function setValue(string $token,int $reposPerRequest = 30){
			$this->token = $token;
			$this->reposPerRequest = $reposPerRequest;
		}

			// https://api.github.com/users/dfg/repos?page=1&per_page=60&my_client_id=1966600cada3e3480640


		public function getInfoUserFromUsername( string $username,string $page) {

			$url = "https://api.github.com/users/".$username."/repos?page=".$page."&per_page=".$this->reposPerRequest.'&my_client_id='.Config::CLIENT_ID_GITHUB.'&client_secret='.Config::PRIVATE_KEY_GITHUB;
			$headers = [
			    'Authorization: token ' . $this->token,
			];

			$result = $this->curlGet($url,$headers);

			return $result;
		}

		public function InfoUser() {
			$url = "https://api.github.com/user/repos";
			$headers = [
			    'Authorization: token ' . $this->token,
			];

			$reposFromToken = $this->curlGet($url,$headers);
			$decode = @json_decode($reposFromToken); // lay dk repos

			if( $decode !== null ) {
				// fail
				if ( isset( $decode->message ) || count($decode) == 0 ) {
					return [
						'status' => 0,
						'message'=>'Tài khoản này không đăng nhập được'
					];
				}

				return [
					'status' => 1,
					'username' => $decode[0]->owner->login, // ko dk
					'id_user' => $decode[0]->owner->id,
					'node_id' => $decode[0]->owner->node_id,
					'avatar' => $decode[0]->owner->avatar_url,
				];				
			} 
			return [
					'status' => 0,
					'message'=>'Lỗi kết nối Github',
				];
		}

		private function curlGet($url,$headers) {
			$ch = curl_init();

			curl_setopt_array($ch, [
			    CURLOPT_URL => $url,
			    CURLOPT_HEADER => false,
			    CURLOPT_HTTPHEADER => $headers,
			    CURLOPT_RETURNTRANSFER => true,
			    CURLOPT_USERAGENT => 'http://developer.github.com/v3/',
			]);

			$result = curl_exec($ch);
			curl_close($ch);
			return $result;
		}

	}