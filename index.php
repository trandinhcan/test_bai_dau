<?php
	
	require 'Config.php';
	require 'Controller/ControllerUser.php';
	require 'Controller/ControllerLogin.php';

	if ( !isset($_COOKIE['token_user']) ) {

		$ControllerLogin = new ControllerLogin();
		$ControllerLogin->login();

	} else {

		$tl1 = isset($_GET['tl1']) ? $_GET['tl1'] : '';
		$tl2 = isset($_GET['tl2']) ? $_GET['tl2'] : '';
		
		$ControllerUser = new ControllerUser($_COOKIE['token_user'],$tl1,$tl2);

	}

