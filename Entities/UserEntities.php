<?php

	class UserEntities{

		private $avatar;
		private $id_user;
		private $node_id;
		private $username;


		public function __construct($avatar,$id_user,$node_id,$username){
			$this->avatar = $avatar;
			$this->id_user = $id_user;
			$this->node_id = $node_id;
			$this->username = $username;
		}

		public function getAvatar(){
			return $this->avatar;
		}

		public function setAvatar($avatar){
			$this->avatar = $avatar;
		}

		public function getId_user(){
			return $this->id_user;
		}

		public function setId_user($id_user){
			$this->id_user = $id_user;
		}

		public function getNode_id(){
			return $this->node_id;
		}

		public function setNode_id($node_id){
			$this->node_id = $node_id;
		}

		public function getUsername(){
			return $this->username;
		}

		public function setUsername($username){
			$this->username = $username;
		}

	}