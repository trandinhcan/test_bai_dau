<?php

	class ReposEntities{

		private $idRepos;
		private $nameRepos;
		private $create_at;
		private $username;
		private $stargazers_count;
		private $description;

		public function __construct($idRepos,$nameRepos,$create_at,$username,$stargazers_count,$description) {
			$this->idRepos = $idRepos;
			$this->nameRepos = $nameRepos;
			$this->create_at = $create_at;
			$this->username = $username;
			$this->stargazers_count = $stargazers_count;
			$this->description = $description;
		}

		public function getIdRepos(){
			return $this->idRepos;
		}

		public function setIdRepos($idRepos){
			$this->idRepos = $idRepos;
		}

		public function getNameRepos(){
			return $this->nameRepos;
		}

		public function setNameRepos($nameRepos){
			$this->nameRepos = $nameRepos;
		}

		public function getCreate_at(){
			return $this->create_at;
		}

		public function setCreate_at($create_at){
			$this->create_at = $create_at;
		}

		public function getUsername(){
			return $this->username;
		}

		public function setUsername($username){
			$this->username = $username;
		}

		public function getStargazers_count(){
			return $this->stargazers_count;
		}

		public function setStargazers_count($stargazers_count){
			$this->stargazers_count = $stargazers_count;
		}

		public function getDescription(){
			return $this->description;
		}

		public function setDescription($description){
			$this->description = $description;
		}
	}