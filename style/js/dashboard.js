// $(document).ready(function(){

	var reposLoad = 0;
	var listReposCache = [];
	var reposPerPage = 30;
	var username;
	var countReposShow = 0;

    var id_user = getCookie('id_user');

	function getRepos(username,page = 1) {
    	let url = 'repos-username/'+username+'.html';
        $('.btnLoadMore').addClass('disabled');
        return $.ajax({
        	'url' : '',
        	'type': 'GET',
        	'data' : {'tl1':'repos-username','tl2':username,'tl3':page },
        	success : function(data){}
        }).always(()=>{$('.btnLoadMore').removeClass('disabled');});
    }

  $('.getReposUser').click(function(){

  	$('#response').html('');
  	$('#loadMore').html('');
  	$('#reposLoadcount').text('0');
  	countReposShow = 0;

  	listReposCache= [];

    username = $('.usernameGithub').val();
    username = $.trim(username);

    var pattern = /^[a-zA-Z0-9]+$/;

    if ( !pattern.test(username) ) {
         $.notify("Username không đúng định dạng");
        return;
    }

    getRepos(username).done(function(data){
    	try {
    		data = JSON.parse(data);
            console.log(data);

            if ( data.message === undefined ) {
                data.forEach(element=>listReposCache.push(element));
                processLoadListRepos();
            } else {
                alert('Không tồn tại');
            }
    	} catch(Ex) {
            alert('Không có kho lưu trữ nào');
    	}
	    	
    });
  });

  function plusRepos(number){
  	countReposShow +=  number;
    $('#reposLoadcount').text( countReposShow );
  }

	function  processLoadListRepos(){
		let arrayReposAvai = listReposCache.splice(0,30);

    	arrayReposAvai.forEach(element=>{
    		showRepos(element);
    	});

    	plusRepos(arrayReposAvai.length);

    	if(listReposCache.length > 0 ) {
    		$('#loadMore').html('<button class="btn btn-success  form-control btnLoadMore">Load More</button>');
    		loadMore();
    	} else {
    		$('#loadMore').html('');	
    	}
	}

    function loadMore() {
    	$('.btnLoadMore').click(function(){
    		let lengthReposCache = listReposCache.length;
    		
    		if ( lengthReposCache < reposPerPage ) { // < 30

    			listReposCache.forEach(element=>showRepos(element));
    			$('#loadMore').html('');
    			plusRepos(lengthReposCache);

    		} else {
    			if ( lengthReposCache == 30 ) {
    				page = Math.ceil(countReposShow / 60); // 60  repos per request get github

    				getRepos(username,page).done(function(data){
                        try {
                            data = JSON.parse(data);

                            if ( data.message === undefined ) {
                                data.forEach(element=>listReposCache.push(element));
                                processLoadListRepos();
                            } else {
                                alert('Không tồn tại');
                            }
                        } catch(Ex) {
                            alert('Không có kho lưu trữ nào');
                        }
				    });
    			} else {
    				processLoadListRepos();
    			}
    		}
    	});	
    }


    function showRepos(element) {
    	$('#response').append(' <div class="col-12">'
                      +'<div style="float:left;width:80%">'
                        +'<h4>'+element.name+'</h4>'
                        +'<h6>'+element.description+'</h6>'
                      +'</div>'
                      +'<div style="float:right;width: 20%">'
                        +'stargazers : <span id="stargazers" style="color:green">'+element.stargazers_count+'</span><br>'
                        +'<button class="btn btn-primary" onclick="saveReposDb(this)" id_repos="'+element.id+'" created_at="'+element.created_at+'" stargazers_count="'+element.stargazers_count+'" name = "'+element.name+'" description="'+element.description+'" nameLogin="'+element.owner.login+'" >Lưu DB</button>'
                      +'</div>'
                    +'<div class="clearfix"></div>'
                    +'<hr/>'
                    +'</div>');
    }

    function saveReposDb(element) {

        let name_repos = $(element).attr('name');
        var usernammeLogin = $(element).attr('nameLogin');
        let stargazers_count = $(element).attr('stargazers_count');
        let created_at = $(element).attr('created_at');
        let id_repos = $(element).attr('id_repos');
        let description = $(element).attr('description');

        $(element).addClass('disabled');
        $.post('save-info-repos.html',{
            'id_repos' : id_repos,
            'name_repos' : name_repos,
            'created_at' : created_at,
            'username' : usernammeLogin,
            'stargazers_count' : name_repos,
            'description' : description,
        }).done(function(data){
            try{
                
                data = JSON.parse(data);
                if( data.status === 1 ) {
                    
                    $(element).text('Đã Lưu');
                    $(element).removeAttr('onclick');
                    $(element).removeClass('btn-primary');
                    $(element).addClass('btn-success');
                    $.notify("Thành Công","success");

                } else{
                    $(element).removeClass('disabled');
                    alert(data.message);
                }
            }catch(ex){
                alert('Fail !');
                $(element).removeClass('disabled');
            }

            
        });



    }

function getCookie(cname) {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(';');
  for(var i = 0; i <ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}


$('.username span').text(getCookie('username'));
$('.id_user span').html(id_user);
