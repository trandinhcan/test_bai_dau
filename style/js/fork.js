
function fork(element){
	var idRepos =  $(element).attr('idRepos');
	var nameRepos = $(element).attr('pathFork');

	$(element).addClass('disabled');
	$(element).removeAttr('onclick');

	client.emit('fork',{idRepos,nameRepos,token});
}

	client.on('fork_success',function(data){
		$('.fork[idRepos="'+data.idRepos+'"]').removeClass('btn-primary');
		$('.fork[idRepos="'+data.idRepos+'"]').css('background-color','#28a745');
		$('.fork[idRepos="'+data.idRepos+'"]').removeClass('disabled');
		$('.fork[idRepos="'+data.idRepos+'"]').html('<a target="_blank" href="https://github.com/'+data.full_name+'">Link Fork Repos</a>');
		alertSuccess(data.full_name);
	});

	client.on('fork_fail',function(data){
		alert(data.message);
	});

	function alertSuccess(link_repos){
		
		let html  = ' <div class="alert text-success" onclick="redirectRepos(\''+link_repos+'\')"><span>Thành Công</span></div>';
		$('.list_repos').prepend(html);
		setTimeout(()=>{$('.alert').remove()},6000);

	}
	function redirectRepos(link){
		$('.alert').html('Your fork is done and this is your new fork repo:<a style="color:green !important" target="_blank" href="https://github.com/'+link+'">Click hrer</a>');
	}
